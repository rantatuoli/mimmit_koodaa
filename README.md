# mimmit-koodaa

> A Vue.js project

# steps

1. basic webpack template
2. filename renamed, new logo
3. install vue-resource (npm install vue-resource) https://www.npmjs.com/package/vue-resource
4. get product json and show product names
5. show product image, name and price inside div-wrapper
6. create category filter and filter products accordingly
7. create a route to product page, show products details in that page

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
