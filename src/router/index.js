import Vue from 'vue'
import Router from 'vue-router'
import Catalog from '@/components/Catalog'
import Product from '@/components/Product'
import vueResource from 'vue-resource'

Vue.use(Router)
Vue.use(vueResource)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Catalog',
      component: Catalog
    },
    {
      path: '/product/:id',
      name: 'Product',
      component: Product,
      props: true
    }
  ]
})
